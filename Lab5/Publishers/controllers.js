const express = require('express');

const PublishersService = require('./services.js');
const { validateFields } = require('../utils');
const { ServerError } = require('../errors');
const router = express.Router();

router.get('/', async (req, res, next) => {
    try {
      const publishers = await PublishersService.getAll();
      const names = publishers.map(item => item.name);
      console.log(publishers);
      res.json(names);
    } catch (err) {
      // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
      next(err);
    }
  });


router.get('/:id', async (req, res, next) => {
    const { id } = req.params;
  
    try {
      validateFields({
        id: {
          value: id,
          type: 'int'
        }
      });
  
      const publisher = await PublishersService.getById(parseInt(id));
      const name = publisher.map(item => item.name);
      res.json(name);
    } catch (err) {
      // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js
      next(err);
    }
  });

router.post('/', async (req, res, next) => {
    const { name } = req.body;
  
    try {
      const fieldsToBeValidated = {
        name: {
          value: req.query.name,
          type: 'alpha'
        }
      };
      console.log(req.query.name); 
      validateFields(fieldsToBeValidated);
      await PublishersService.add(req.query.name);
      res.status(201).send('Insert ok!' );
    } catch (err) {
      next(err);
    }
  });

router.put('/:id', async (req, res, next) => {
    const { id } = req.params;
    const { name } = req.body;
  
    try {
      const fieldsToBeValidated = {
        id: {
          value: parseInt(id),
          type: 'int'
        },
        name: {
          value: req.query.name,
          type: 'alpha'
        }
      };
      validateFields(fieldsToBeValidated);
      await PublishersService.updateById(parseInt(id), req.query.name);
      res.status(204).send('Update ok!');
    } catch (err) {
      next(err);
    }
  });

router.delete('/:id', async (req, res, next) => {
    const { id } = req.params;
  
    try {
      validateFields({
        id: {
          value: id,
          type: 'int'
        }
      });
  
      await PublishersService.deleteById(parseInt(id));
      res.status(204).send('Delete ok!');
    } catch (err) {
      next(err);
    }
  });

module.exports = router;

