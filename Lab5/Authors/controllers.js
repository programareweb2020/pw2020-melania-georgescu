const express = require('express');


const jwt = require('../security/Jwt');
const roles = require('../security/Roles');

const AuthorsService = require('./services.js');
const { validateFields } = require('../utils');
const { ServerError } = require('../errors');

const router = express.Router();

router.get('/', jwt.authorizeAndExtractToken, roles.authorizeRoles("admin", "user"), async (req, res, next) => {

    try {
        const authors = await AuthorsService.getAll();
        res.json(authors);

    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});

router.get('/:id', jwt.authorizeAndExtractToken, roles.authorizeRoles("admin", "user"), async (req, res, next) => {
    const {
        id
    } = req.params;
    try {

        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });
        const author = await AuthorsService.getById(parseInt(id));
        res.json(author);
    } catch (err) {
        // daca primesc eroare, pasez eroarea mai departe la handler-ul de errori declarat ca middleware in start.js 
        next(err);
    }
});


router.post('/', jwt.authorizeAndExtractToken, roles.authorizeRoles("admin"), async (req, res, next) => {
    const {
        first_name,
        last_name
    } = req.body;

    // validare de campuri
    try {

        const fieldsToBeValidated = {
            first_name: {
                value: first_name,
                type: 'alpha'
            },
            last_name: {
                value: last_name,
                type: 'alpha'
            }
        };
	console.log(req.query.first_name);
        validateFields(fieldsToBeValidated);

        await AuthorsService.add(req.query.first_name, req.query.last_name);

        res.status(201).end();
    } catch (err) {
        next(err);
    }
});

router.put('/:id', jwt.authorizeAndExtractToken, roles.authorizeRoles("admin"), async (req, res, next) => {
    const {
        id
    } = req.params;
    const {
        first_name,
        last_name
    } = req.body;
    try {

        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'int'
            },
            first_name: {
                value: first_name,
                type: 'alpha'
            },
            last_name: {
                value: last_name,
                type: 'alpha'
            }
        };

        validateFields(fieldsToBeValidated);

        await AuthorsService.updateById(parseInt(id), first_name, last_name);
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', jwt.authorizeAndExtractToken, roles.authorizeRoles("admin"), async (req, res, next) => {
    const {
        id
    } = req.params;

    try {

        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });
        await AuthorsService.deleteById(parseInt(id));
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});

module.exports = router;
