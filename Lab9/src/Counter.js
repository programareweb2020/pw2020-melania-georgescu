import React from 'react';
import { useState, useEffect } from 'react';


  function Counter() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    if (count === 0) {
      alert("ALERT!!! counter = 0");
    }
  });

  
 
    return (
   <div>
   <button className='inc' onClick={(e) => setCount(count + 1)}>Increment!</button>
    <button className='dec' onClick={(e) => setCount(count - 1)}>Decrement!</button>
    <button className='reset' onClick={(e) => setCount(0)}>Reset</button>
    <h1>Current Count: {count}</h1>
  </div>
    );
  
}

export default Counter;