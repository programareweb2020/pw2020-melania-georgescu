
console.log("----------Task1---------")
console.log("hello world!")

console.log("----------Task2---------")
var today = new Date();
var dd = today.getDate();

var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
if(dd<10) 
{
    dd='0'+dd;
} 

if(mm<10) 
{
    mm='0'+mm;
} 
today = dd+'-'+mm+'-'+yyyy;
console.log(today);

console.log("----------Task3---------")
for (var a=[],i=0;i<100;++i) {
    a[i]=Math.floor(Math.random() * 101);       
}
const b = a.filter(x => x%2 === 0);

console.log("The full array: " + a + '\n');
console.log("Even elements: " + b);


console.log("----------Task4---------")
function gf(a, i){
    console.log(a[i])
}

function vec (a,i, gf){
    gf(a, i);
}  

vec(a, 3, gf);