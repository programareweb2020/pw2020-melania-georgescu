import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Nav from './Nav';
import Counter from './Counter';

class Layout extends React.Component {
    render() {
        return (
            <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)'
            }}>
           
                <Header>
                    <h1 style={{ color: 'red' }}>This is the header</h1>
                </Header>
                <Nav title='This is the nav section' bodyText='Another day in isolation. Everything is terrible and I am very sad.'/>
                <Counter></Counter> 
                <Footer>
                    <h1 style={{ color: 'red' }}>This is the footer. Byeee!</h1>
                </Footer>    
            </div>
        )
    }
}

export default Layout;
