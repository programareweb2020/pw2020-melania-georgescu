const {
    Books,
    Authors
} = require('../data');

const add = async (name, authorId, genres) => {
    // create new Book obj
    // save it
    const book = new Books({
        name: name,
        author: authorId,
        genres: genres
    });
    await book.save();
};

const getAll = async () => {
    // get all books
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    books =  await Books.find().populate('author');
    return books.map(book => { return {
        _id: book._id,
        name: book.name,
        author: book.author.firstName + " " + book.author.lastName,
        genres: book.genres
    }});
};

const getById = async (id) => {
    // get book by id
    // populate 'author' field
    // modify output so author is made of 'author.firstName author.lastName'
    let book = await Books.findById(id).populate('author');
    return {
        _id: book._id,
        name: book.name,
        author: book.author.firstName + " " + book.author.lastName,
        genres: book.genres
    };
};

const getByAuthorId = async (id) => {
    // get book by author id
    // modify output so author is made of 'author.firstName author.lastName'
    books = Books.find({ author: id }).populate('author');
    return books.map(book => { return {
        _id: book._id,
        name: book.name,
        author: book.author.firstName + " " + book.author.lastName,
        genres: book.genres
    }});
};

const updateById = async (id, name, authorId, genres) => {
    // update by id
    await Books.findByIdAndUpdate(id, { 
        name: name, 
        autor: authorId,
        genres: genres 
    });
};

const deleteById = async (id) => {
    // delete by id
    await Books.findByIdAndDelete(id);
};

module.exports = {
    add,
    getAll,
    getById,
    getByAuthorId,
    updateById,
    deleteById
}